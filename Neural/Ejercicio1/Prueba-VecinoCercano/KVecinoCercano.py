import numpy as np
from sklearn import preprocessing, cross_validation, neighbors
import pandas as pd
from sklearn import utils

df = pd.read_csv('/Users/jesusgarcialicona/Desktop/AvanceEnfermedad.csv.txt')
df.replace('?', -99999, inplace=True)
df.drop(['id'],1,inplace=True)


X = np.array(df.drop(['%_de_Avance'],1))
y = np.array(df['%_de_Avance'])
 
X_train, X_test, y_train, y_test = cross_validation.train_test_split(X,y,test_size=0.2)

lab_enc = preprocessing.LabelEncoder()
training_scores_encoded = lab_enc.fit_transform(y_train)




clf= neighbors.KNeighborsClassifier()
clf.fit(X_train, training_scores_encoded)


clf.score(X_train,training_scores_encoded)


prediction_data_test  = np.array([0.013513514,0.023809524,0.025,0.026315789])
prediction_data_test = prediction_data_test.reshape(1, -1)
result = (clf.predict(prediction_data_test))

print ((result*100)/3988)

______________________________________________________________________

import pandas as pd
from sklearn import linear_model
from sklearn.model_selection import train_test_split

reg = linear_model.LogisticRegression()
archivo = '/Users/jesusgarcialicona/Desktop/AvanceEnfermedad.csv.txt'

df = pd.read_csv(archivo)

arreglox = df[df.columns[:-1]].as_matrix()
arregloy=df[df.columns[-1]].as_matrix()

X_train, X_test, y_train, y_test = train_test_split(arreglox, arregloy)


lab_enc = preprocessing.LabelEncoder()
y_train = lab_enc.fit_transform(y_train)


reg.fit(X_train,y_train)

print (reg.score(X_test,y_test)