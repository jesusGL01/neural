from pandas import DataFrame
from sklearn import linear_model
import tkinter as tk 
import statsmodels.api as sm
import pandas as pd

Stock_Market = pd.read_csv(r'/Users/jesusgarcialicona/Desktop/AvanceEnfermedad.csv.txt')
df = DataFrame(Stock_Market,columns=['Sustancia_1','Sustancia_2','Sustancia_3','Sustancia_4','%_de_Avance'])


X = df[['Sustancia_1','Sustancia_2','Sustancia_3','Sustancia_4']] # here we have 2 variables for multiple regression. If you just want to use one variable for simple linear regression, then use X = df['Interest_Rate'] for example.Alternatively, you may add additional variables within the brackets
Y = df['%_de_Avance']


# with sklearn
regr = linear_model.LinearRegression()
regr.fit(X, Y)

print('Intercept: \n', regr.intercept_)
print('Coefficients: \n', regr.coef_)


# with statsmodels
X = sm.add_constant(X) # adding a constant
 
model = sm.OLS(Y, X).fit()
predictions = model.predict(X) 

# tkinter GUI
root= tk.Tk() 
 
canvas1 = tk.Canvas(root, width = 1200, height = 450)
canvas1.pack()

# with sklearn
Intercept_result = ('Intercept: ', regr.intercept_)
label_Intercept = tk.Label(root, text=Intercept_result, justify = 'center')
canvas1.create_window(260, 220, window=label_Intercept)

# with sklearn
Coefficients_result  = ('Coefficients: ', regr.coef_)
label_Coefficients = tk.Label(root, text=Coefficients_result, justify = 'center')
canvas1.create_window(260, 240, window=label_Coefficients)

# with statsmodels
print_model = model.summary()
label_model = tk.Label(root, text=print_model, justify = 'center', relief = 'solid', bg='LightSkyBlue1')
canvas1.create_window(800, 220, window=label_model)


# New_Interest_Rate label and input box
label1 = tk.Label(root, text='Sustancia_1: ')
canvas1.create_window(100, 100, window=label1)

entry1 = tk.Entry (root) # create 1st entry box
canvas1.create_window(270, 100, window=entry1)

# New_Unemployment_Rate label and input box
label2 = tk.Label(root, text=' Sustancia_2: ')
canvas1.create_window(100, 120, window=label2)

entry2 = tk.Entry (root) # create 2nd entry box
canvas1.create_window(270, 120, window=entry2)

# New_Unemployment_Rate label and input box
label3 = tk.Label(root, text=' Sustancia_3: ')
canvas1.create_window(100, 140, window=label3)

entry3 = tk.Entry (root) # create 3nd entry box
canvas1.create_window(270, 140, window=entry3)

# New_Unemployment_Rate label and input box
label4 = tk.Label(root, text=' Sustancia_4: ')
canvas1.create_window(100, 160, window=label4)

entry4 = tk.Entry (root) # create 4nd entry box
canvas1.create_window(270, 160, window=entry4)



def values(): 
    global Sustancia_1 #our 1st input variable
    Sustancia_1 = float(entry1.get()) 
    
    global Sustancia_2  #our 2nd input variable
    Sustancia_2  = float(entry2.get()) 
    
    global Sustancia_3  #our 3nd input variable
    Sustancia_3  = float(entry3.get()) 
    
    global Sustancia_4  #our 4nd input variable
    Sustancia_4  = float(entry4.get()) 
    
    Prediction_result  = ('Predicción de porcentaje de enfermedad: ', regr.predict([[Sustancia_1  ,Sustancia_2, Sustancia_3, Sustancia_4 ]]))
    label_Prediction = tk.Label(root, text= Prediction_result, bg='orange')
    canvas1.create_window(150, 380, window=label_Prediction)
    
button1 = tk.Button (root, text='Predicción de porcentaje de enfermedad',command=values, bg='orange') # button to call the 'values' command above 
canvas1.create_window(150, 350, window=button1)
 

root.mainloop()