# _NEURAL
Hemos establecido el test para medir tus habilidades, no necesariamente tienen que ver con lo que has aprendido en la universidad sino con tu capacidad de encontrar soluciones, de investigar y sobre todo de aplicarlas adecuadamente para resolver problemas, sin embargo, tu formación académica sí repercute en cómo resuelves algo, que es lo que deseamos evaluar de ti. En la ciencia de datos normalmente no hay guías de cómo resolver un problema en específico, pero sí muchísimos insights en la red, esto es una pequeña muestra de lo que nos enfrentamos diariamente.
A continuación, te planeamos 3 pruebas, usa todas las herramientas que tengas a tu disposición y que conozcas, haz lucir tus habilidades, lo que nos interesa es tu capacidad y lo que traigas para proponer.
1. El equipo de Neural recibió información histórica sobre la relación entre cuatro sustancias químicas encontradas en el cuerpo humano y el porcentaje de avance de cierta enfermedad al momento de tomar las muestras. Cuando esas muestras son tomadas, el laboratorio tarda cinco semanas en dar el porcentaje de avance de la enfermedad, esto hace que la reacción médica sea muy lenta para iniciar un tratamiento. Por lo tanto, se te pide que con la información histórica recolectada puedas determinar una forma de modelar el porcentaje de avance de la enfermedad y así cuando se presenten nuevos casos se aplique tus análisis sobre sus muestras para tener un diagnóstico del avance inmediato y evitar tener que hacer un estudio de laboratorio tardío. La base Avance enfermedad.csv consta de 5,000 datos que contienen en cada columna:
• Número de identificación del paciente: ID Paciente
• Los valores numéricos de cada sustancia encontrada: Sustancia 1, Sustancia 2, Sustancia 3 y Sustancia 4.
• Porcentaje de avance de la enfermedad al momento de la toma de muestra: % de Avance.
2. Se requiere un mecanismo para alertar, vía correo electrónico, el momento en que una computadora con Windows 10 pierde internet o energía eléctrica para notificar inmediatamente al equipo de emergencias que sus procesos se han detenido y puedan abordar el problema lo más pronto posible. Por lo tanto, se te pide que generes algún proceso para avisar el momento en que la computadora pierde señal y notificar al equipo de emergencias por email.
3. Un cliente requiere un formulario web para enviar datos importantes los cuales deberán ser con el siguiente orden:
• Nombre, Apellido Paterno, Apellido Materno.
• Fecha de Nacimiento.
• Email.
• Teléfono Móvil.
• Nacionalidad.
• Dos espacios de para subir imagen de la INE/IFE y comprobante domiciliario.
• Un checkbox con el texto “Acepto el Aviso de Privacidad y los Términos y Condiciones del uso del sitio.”
Los datos y las imágenes deben de almacenarse una vez terminada la captura de información.